# Example in https://www.tensorflow.org/tutorials/keras/basic_text_classification

#1. Imports
import tensorflow as tf
from tensorflow import keras
import numpy as np
print (tf.__version__)

#2. Download dataset
imdb = keras.datasets.imdb
#Load dataset
(train_data, train_labels), (test_data, test_labels) = imdb.load_data(num_words=10000) 
#top 10,000 most freq words for each reqview which contains lot of words

#Explore the data
print("Testing entries: {}, labels: {}".format(len(test_data), len(test_labels)))
print("Training entries: {}, labels: {}".format(len(train_data), len(train_labels)))
#type(train_data)
#<class 'numpy.ndarray'>

print(train_data[0])
print(len(train_data[0]))
#218 
#type(train_data[0])
#<class 'list'>

print(len(train_data[0]), len(train_data[1]), len(train_data[2]), len(train_data[3]))
#218 189 141 550


# Convert the integers back to words
# A dictionary  - mapping words to an integer index
word_index = imdb.get_word_index()
#type(word_index)
#<class 'dict'>

#type(word_index.items)
#<class 'dic_itemst'>
#len(word_index.items())
#88584 items
#Example of dict items: ('paget', 18509), ('expands', 20597)])


# The first indices are reserved
word_index = {k:(v+3) for k,v in word_index.items()} #Move all indexes of all dictionary items by 3  
word_index["<PAD>"] = 0    #Create your own dictionary entry for index 0
word_index["<START>"] = 1
word_index["<UNK>"] = 2  # unknown
word_index["<UNUSED>"] = 3

reverse_word_index = dict([(value, key) for (key, value) in word_index.items()])

def decode_review(text):
    return ' '.join([reverse_word_index.get(i, '?') for i in text])

#read a review
decode_review(train_data[0])

#<START> this film was just brilliant casting location scenery story direction everyone's really suited the part they played and you could just imagine being there robert <UNK> is an amazing actor and now the same being director <UNK> father came from the same scottish island as myself so i loved the fact there was a real connection with this film the witty remarks throughout the film were great it was just brilliant so much that i bought the film as soon as it was released for <UNK> and would recommend it to everyone to watch and the fly fishing was amazing really cried at the end it was so sad and you know what they say if you cry at a film it must have been good and this definitely was also <UNK> to the two little boy's that played the <UNK> of norman and paul they were just brilliant children are often left out of the <UNK> list i think because the stars that play them all grown up are such a big profile for the whole film but these children are amazing and should be praised for what they have done don't you think the whole story was so lovely because it was true and was someone's life after all that was shared with us all"

	
train_data = keras.preprocessing.sequence.pad_sequences(train_data,
                                                        value=word_index["<PAD>"],
                                                        padding='post',
                                                        maxlen=256)

test_data = keras.preprocessing.sequence.pad_sequences(test_data,
                                                       value=word_index["<PAD>"],
                                                       padding='post',
                                                       maxlen=256)

print(len(train_data[0]), len(train_data[1]), len(train_data[2]), len(train_data[3]))
#256 256 256 256 
#if the review is bigger than 256, then clip off the remaining. For example, train_Data[3] initially had 550 words. not it has been clipped to keepin only the last 256 words


#Buid the model
# input shape is the vocabulary count used for the movie reviews (10,000 words)
vocab_size = 10000

model = keras.Sequential()
model.add(keras.layers.Embedding(vocab_size, 16)) #First layer -- Vector output dimensions are (bathch sequence, embedding)
model.add(keras.layers.GlobalAveragePooling1D()) #Second layer - Fixed len output layer
model.add(keras.layers.Dense(16, activation=tf.nn.relu)) #Funlly connected dense layer
model.add(keras.layers.Dense(1, activation=tf.nn.sigmoid)) #Densly connected with single output mode


model.summary()

#Loss Optimizer
#A model needs a loss function and an optimizer for training. Since this is a binary classification problem and the model outputs of a probability (a single-unit layer with a sigmoid activation), we'll use the binary_crossentropy loss function.

model.compile(optimizer=tf.train.AdamOptimizer(),
              loss='binary_crossentropy',
              metrics=['accuracy'])

#Create a validation set
#When training, we want to check the accuracy of the model on data it hasn't seen before. 
#Create a validation set by setting apart 10,000 examples from the original training data. 

x_val = train_data[:10000]
partial_x_train = train_data[10000:]

y_val = train_labels[:10000]
partial_y_train = train_labels[10000:]

#Train the model for 40 epochs in mini-batches of 512 samples.
#This is 40 iterations over all samples in the x_train and y_train tensors.
history = model.fit(partial_x_train,
                    partial_y_train,
                    epochs=40,
                    batch_size=512,
                    validation_data=(x_val, y_val),
                    verbose=1)

#Evaluate the model
results = model.evaluate(test_data, test_labels)
print(results)

#Create a graph of accuracy and loss over time
history_dict = history.history
history_dict.keys()
#result: dict_keys(['val_loss', 'acc', 'loss', 'val_acc'])

#Plotting graphs
import matplotlib.pyplot as plt

acc = history.history['acc']
val_acc = history.history['val_acc']
loss = history.history['loss']
val_loss = history.history['val_loss']

epochs = range(1, len(acc) + 1)

# "bo" is for "blue dot"
plt.plot(epochs, loss, 'bo', label='Training loss')
# b is for "solid blue line"
plt.plot(epochs, val_loss, 'b', label='Validation loss')
plt.title('Training and validation loss')
plt.xlabel('Epochs')
plt.ylabel('Loss')
plt.legend()

plt.show()


plt.clf()   # clear figure
acc_values = history_dict['acc']
val_acc_values = history_dict['val_acc']

plt.plot(epochs, acc, 'bo', label='Training acc')
plt.plot(epochs, val_acc, 'b', label='Validation acc')
plt.title('Training and validation accuracy')
plt.xlabel('Epochs')
plt.ylabel('Accuracy')
plt.legend()

plt.show()